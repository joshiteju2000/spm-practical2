# Q5.Write a python program to read three numbers and find the smallest among them.

number1 = int(input("Enter first number"))
number2 = int(input("Enter second number"))
number3 = int(input("Enter third number"))

if number1<number2 and number1<number3:
    print("number1 is smaller")
elif number2<number1 and number2<number3:
    print("number2 is smaller")
else:
    print("number3 is smaller")
